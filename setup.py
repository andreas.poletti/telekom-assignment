from setuptools import setup

setup(
    name='Bookstore API',
    version='0.1',
    packages=['app'],
    url='https://poletti.me',
    license='',
    author='Andreas Poletti',
    author_email='andreas@poletti.me',
    description='',
    install_requires=[
        'fastapi~=0.109.0',
        'uvicorn~=0.25.0',
        'setuptools~=69.0.2',
        'pydantic~=2.5.3',
        'pydantic-settings~=2.1.0',
        'psycopg-binary~=3.1.17',
        'psycopg~=3.1.17'
    ]
)

# DevOps Engineer Assignment

A junior developer has given you his code for a bookstore API, and he claims that
the client (a bookstore) insists it's deployed on our k8s cluster, otherwise they will not pay
for all the hard work you and your developer coworkers have done.

Your job is to prepare this API to be deployed on a Kubernetes cluster and <em>(optionally)
for easier local development as a containerized application.</em>

### The application needs:

- to be safe as possible
- run in multiple containers with all its dependencies
- to have easily adjustable settings (or parameters)
- to have an option to directly view (or edit) raw data in the backend database
- (optionally) to be easily tested in a local environment
  without k8s installed

Good luck and have fun!

Assignment Documentation
========================

This README provides comprehensive details about the local development setup, application updates, deployment strategies, and security measures for the project.

Local Development
-----------------

### Running the Application in Docker

-   **Docker Compose:** Use `docker-compose up -d` to start the application. This setup automatically populates the database.
-   **Accessing pgAdmin4:** pgAdmin4 is available at `http://localhost:8080`.

### Configuration Files

-   **Environment Configuration:** Use `app.env` and `db.env` to configure the application and database within Docker Compose.

Application Updates
-------------------

### Dependencies and Configuration

-   **Updated Dependencies:** The application now supports the latest versions of all dependencies.
-   **Configuration Enhancements:**
    -   Utilizes the latest `pydantic` version for `config.py`.
    -   Improved code clarity in `config.py`.
    -   Added support for reading environment variables from an `.env` file.

Personal Implementation Notes
-----------------------------

### Backend Data Access

-   **pgAdmin4 Integration:** Included in the Docker Compose setup to facilitate direct access to backend database data.
-   **CRUD Interface:** Not implemented, considering the API nature of the project.
-   **Kubernetes Deployment:** Excluded `pgadmin4` for security reasons.

Application Environment Variables
---------------------------------

| Variable          | Description                                     |
|-------------------|-------------------------------------------------|
| `APP_DB`          | Database name                                   |
| `APP_DB_HOST`     | Database server host                            |
| `APP_DB_PORT`     | Database server port                            |
| `APP_DB_USER`     | Database user                                   |
| `APP_DB_PASSWORD` | Database password                               |
| `APP_DB_SCHEMA`   | Database schema                                 |
| `APP_API_PORT`    | API server port                                 |
| `APP_API_HOST`    | API server host                                 |
| `APP_DEBUG`       | Enable (`True`) or disable (`False`) autoreload |

Docker Integration
------------------

-   **Dockerization:** The application is dockerized without a multi-stage build, as it's not required for this project.

Development Features
--------------------

### Reload on Save

-   **Auto-reload:** Enabled when the debug flag is set to `True`.
-   **Local Development Note:** Auto-reload is beneficial for local development, but it limits debugging capabilities like setting breakpoints in Docker.

Deployment Strategy
-------------------

### Kubernetes Deployment

-   **Deployment Method:** Utilizes `Helm` for a basic yet efficient Kubernetes deployment.
-   **Database Management:** Recommends using a separate cloud-managed database service instead of deploying Postgres within the same Helm chart or separately. If needed, please see Postgres Configuration section

### Ingress Considerations

-   **Ingress Setup:** Suggests using cloud-native ingresses or NGINX/Azure Application Gateway Ingress Controller.
-   **Note:** The status of native Kubernetes ingress for Azure is uncertain, hence the recommendation for alternative solutions.

### Custom Helm Variables

| Variable                             | Description                          |
|--------------------------------------|--------------------------------------|
| `cloud.keyvaultName`                 | Keyvault name                        |
| `cloud.clientId`                     | Federated Identity Client ID         |
| `cloud.identityTenant`               | Tenant ID                            |
| `secrets.applicationEnvironmentName` | Secret name in the vault for the app |

### Secrets Management

-   **Azure Secrets:** Implements Azure Secrets Store CSI Driver, authenticated via Workload Identity.

### Workload Identity

-   **Setup Requirement:** Requires setup as per [Microsoft's documentation](https://learn.microsoft.com/en-us/azure/aks/csi-secrets-store-driver).

### Security Measures

-   **Container Security:** Runs with an `application` user, restricting access.
-   **Kubernetes Security Contexts:** Enforces the use of unprivileged users and read-only filesystems for pods.


#### Postgres Configuration

First the `Postgres` configuration.

postgres-config.yaml:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgres-config
  labels:
    app: postgres
data:
  POSTGRES_DB: "bookstore"
  POSTGRES_USER: "bookstore"
  POSTGRES_PASSWORD: "test"
```

In order to have persistent data, we use a persistent volume and a persistent volume claim.

postgres-pvc-pv.yaml:

```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: postgres-pv-volume
  labels:
    type: local
    app: postgres
spec:
  storageClassName: manual
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  hostPath:
    path: "/mnt/data"
---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: postgres-pv-claim
  labels:
    app: postgres
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
```

The `postgres` deployment:

postgres-deployment.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: postgres  # Sets Deployment name
spec:
  replicas: 1
  selector:
    matchLabels:
      app: postgres
  template:
    metadata:
      labels:
        app: postgres
    spec:
      containers:
        - name: postgres
          image: postgres:16
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5432
          envFrom:
            - configMapRef:
                name: postgres-config
          volumeMounts:
            - mountPath: /var/lib/postgresql/data
              name: postgredb
      volumes:
        - name: postgredb
          persistentVolumeClaim:
            claimName: postgres-pv-claim
```

And finally its service

postgres-service.yaml:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres
  labels:
    app: postgres
spec:
  type: NodePort
  ports:
    - port: 5432
  selector:
    app: postgres
```
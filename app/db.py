import psycopg
from psycopg import sql
from psycopg.rows import dict_row
from fastapi.responses import JSONResponse


DEFAULT_FIELDS = ("name", "year", "author")


# Updated to latest version of psycopg
def connect(database, host, user, password, port):
    # Corrected the connection string
    conn = psycopg.connect(dbname=database, host=host, user=user, password=password, port=port)
    return conn


def query_db(conn, schema, table, select_fields=DEFAULT_FIELDS, name=None, year=None, author=None, limit=100):
    with conn.cursor(row_factory=dict_row) as cur:
        query = sql.SQL("SELECT {fields} FROM {schema}.{table} WHERE "
                        "name = COALESCE(%s, name) AND "
                        "year = COALESCE(%s, year) AND "
                        "author = COALESCE(%s, author) LIMIT %s").format(
            fields=sql.SQL(', ').join(sql.Identifier(f) for f in select_fields),
            schema=sql.Identifier(schema),
            table=sql.Identifier(table),
        )

        # Executing the query
        cur.execute(query, (name, year, author, limit))
        result = cur.fetchall()

    # Returning the result as a JSON response
    return JSONResponse(result)

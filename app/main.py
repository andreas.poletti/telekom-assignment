import uvicorn
from fastapi import FastAPI
from fastapi.responses import JSONResponse

from config import settings
from db import connect, query_db

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Welcome to the bookstore!"}


@app.get("/book/")
async def book(
        name: str = None, year: int = None, author: str = None, limit: int = 100
):
    try:
        conn = connect(
            database=settings.db,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_user,
            password=settings.db_password,
        )
        result = query_db(
            conn,
            settings.db_schema,
            "book",
            ("name", "author", "year"),
            name=name,
            year=year,
            author=author,
            limit=limit,
        )
        # We already get a JSONResponse. Trying to return a JSONResponse here leads to exception:
        # Object of type JSONResponse is not JSON serializable
        return result
    except Exception as e:
        return JSONResponse({"message": str(e)}, status_code=500)


@app.get("/db_info/")
def db_info():
    try:
        connect(
            database=settings.db,
            host=settings.db_host,
            port=settings.db_port,
            user=settings.db_user,
            password=settings.db_password,
        )
        return {
            "message": f"Connected to {settings.db_host}:{settings.db_port}/{settings.db}. User: {settings.db_user}."
        }
    except Exception as e:
        return JSONResponse(
            {
                "message": f"{str(e)} {settings.db_host}:{settings.db_port}/{settings.db}. User: {settings.db_user}."
            },
            status_code=500,
        )


if __name__ == "__main__":
    uvicorn.run("main:app",
                host=settings.api_host,
                port=settings.api_port,
                reload=True if settings.debug else False)

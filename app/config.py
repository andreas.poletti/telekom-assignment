from pydantic_settings import BaseSettings


# Upgraded from pydantic 1 to pydantic 2, automatically handles env variables
# Renamed attributes to handle multiple types of env variables (app-specific/db-specific)
class Settings(BaseSettings):
    db: str = "bookstore"
    db_host: str = "localhost"
    db_port: int = 5432
    db_user: str = "bookstore"
    db_password: str = "test"
    db_schema: str = "bookstore"
    api_port: int = 8000
    api_host: str = "0.0.0.0"
    debug: bool = False

    class Config:
        env_prefix = "APP_"  # Prefix to use for environment variables
        env_file = ".env"  # If we want to use an env file


settings = Settings()

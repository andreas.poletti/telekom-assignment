#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "bookstore" --dbname "bookstore" <<-EOSQL
  CREATE SCHEMA ${POSTGRES_SCHEMA};
  CREATE TABLE IF NOT EXISTS ${POSTGRES_SCHEMA}.book (
      id serial primary key,
      name text not null,
      year int not null,
      author text not null
  );

  INSERT INTO ${POSTGRES_SCHEMA}.book
      (name, author, year)
  VALUES
      ('Terry Pratchett, Otec prasátek', 'Bill Kaye, Stephen Player', 2007),
      ('TERRY PRATCHETT DISCWORLD NOVEL COLLECTION.', 'TERRY. PRATCHETT', 2012),
      ('Terry Pratchett''s Discworld Coloring Book', 'Terry Pratchett', 2017),
      ('Sourcery', 'Terry Pratchett', 2014),
      ('Dodger', 'Terry Pratchett', 2019),
      ('Seriously Funny', 'Terry Pratchett', 2016),
      ('Nation', 'Terry Pratchett', 2009),
      ('The Carpet People', 'Terry Pratchett', 2015),
      ('Truckers', 'Terry Pratchett', 2015),
      ('Sekáč', 'Terry Pratchett, Jan Kantůrek', 2011);
EOSQL
FROM python:3.12.1-alpine3.19

# Going above and beyond, even though usually people don't do this
# Build args
ARG APP_UID=1000
ARG APP_GID=1000
ARG APP_GROUP_NAME=application
ARG APP_USERNAME=application

# Setting default envs for image. Although useless in this context, I also included a conditional env variable.
# App env
ARG DEBUG

ENV APP_DB=book \
    APP_DB_HOST=localhost \
    APP_DB_PORT=5432 \
    APP_DB_USER=postgres \
    APP_DB_PASSWORD=test \
    APP_DB_SCHEMA=bookstore \
    APP_API_PORT=8000 \
    APP_API_HOST=0.0.0.0 \
    APP_DEBUG=False


# Create group for application
RUN addgroup ${APP_GROUP_NAME} -g ${APP_GID}

# Create user for application with no home and disabled password, in previously created group
RUN adduser -g "application" ${APP_USERNAME} -u ${APP_UID} -G ${APP_GROUP_NAME} -D -H

RUN mkdir /app

COPY app /app

COPY requirements.txt /requirements.txt

RUN pip install -r requirements.txt && rm /requirements.txt

WORKDIR /app

# Setting UID instead of username as Kubernetes restrictions do not take into account username, only UID, needs != 0
USER ${APP_UID}

EXPOSE ${API_PORT}

ENTRYPOINT ["python", "main.py"]